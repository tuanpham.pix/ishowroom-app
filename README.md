I. INTRODUCTION.
A. Background information.
- E-commerce is a modern business practice that responds to the demands of companies, suppliers, and clients to lower costs, raise product and service quality, and quicken delivery times. The following methods of exchanging commercial information without paper are referred to as E-commerce such as Electronic Data Interchange, Electronic Mail, Electronic Fund Transfer, Electronic Bulletin Boards and other Network-based technologies. [1]

- E-commerce can provide some kind of following features such as:
+ Non-Cash payment: E-commerce transfers via bank websites, credit cards, debit cards, smart 	cards, and other forms of electronic payment are all possible through E-commerce. [2]
+ 24/7 Service availability: Business operations and customer service delivery are automated 	through E-commerce. Whenever, anyplace, it is accessible. [2]
+ Advertising and Marketing: Advertising for businesses' goods and services has a wider 	audience thanks to E-commerce. Better product/service marketing management is facilitated by it.
+ Improved sales: Orders for the items may be created through E-commerce whenever and 	wherever they are needed, without the need for any human involvement. Current sales 	volumes receive a significant increase as a result. [2]
+ Inventory management: Online shopping streamlines inventory control. When needed, 	reports are created right away. It becomes incredibly simple and effective to manage product 	inventories. [2]
Because of E-commerce’s convenient above features, it nearly replace with the Traditional E-commerce. Therefore, the following table will show the comparison between Traditional E-commerce and Modern E-commerce. 

- A great deal of reliance on interpersonal information sharing.	Electronic channels for communication make it simple to share information, reducing the need for face-to-face conversation.

B. Research problem and objectives.
- In the fiercely competitive E-commerce landscape in recent years, a business's ability to attract and maintain the number of user involvement is critical to its success. Online buyers are less engaged and satisfied when traditional 2D product displays and static photos fall short of offering an immersive and customized purchasing experience. Though significant progress has been made in digital marketing and better performance of UI design, however, a little information about how interactive 3D product customization might improve consumer engagement and affect purchase behavior. In order to solve this problem, this study will explore how 3D customization tools affect consumer engagement, satisfaction, and purchase behavior in E-commerce.
- This study's main goal is to evaluate how well interactive 3D product modification may increase consumer interaction on E-commerce platforms. Consequently, the research will focus on the following particular goals:
+ Assess User Experience: compare typical 2D product displays with interactive 3D product 			customization to see how it impacts the customer experience overall.
+ Measure Customer Satisfaction: review the impact of 3D customization tools on customer 			satisfaction levels during the online shopping process.
+ Analyze Purchase Intentions: Investigate the influence of 3D product customization on 			customers' purchase intentions and behavior.
+ Identify Key Factors: Identify and analyze the key factors that contribute to the effectiveness 			of 3D customization tools, such as user interface design, ease of use, and perceived value.
+ Integrating payment method: utilization all the payment method as a normal 2D product.
- By addressing these goals, the study will provide light on how interactive 3D technologies function in E-commerce and provide useful advice for improving the online buying experience.
C. Significance of the Study.
- There are several reasons why this study, which focuses on using interactive 3D product modification to improve e-commerce customer engagement, is important. Firstly, increasing the number of information and knowledge  in the literature on digital marketing and user experience design, it will enhance academic research. Although there are several studies has been done on many other of E-commerce and customer interaction, there is not much empirical data on how well 3D customization tools work to improve user involvement and satisfaction.
- Practically, the findings of this research have important implications for e-commerce businesses seeking to differentiate themselves in a highly competitive market. By integrating interactive 3D product customization features, online retailers can provide a more immersive and personalized shopping experience, which has been shown to increase customer satisfaction and purchase intentions.
- Furthermore, this study emphasizes how important innovation and technical development are to the E-commerce industry. The study encourages more research and use of innovative interactive technologies in online shopping by demonstrating the useful applications and advantages of 3D customizing technology.
- Finally, this research provides the foundation for further studies in related fields. It creates opportunities to look at the long-term consequences of 3D customization on consumer behavior, investigate various product categories and customization decisions, and integrate new technologies like virtual reality (VR) and augmented reality (AR) into e-commerce platforms.


