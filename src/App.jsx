import "./App.css";

function App() {
    return (
        <>
            <div className="container w-full mx-auto h-screen relative">
                <a
                    href="#"
                    className="fixed xl:bottom-8 xl:right-40 xs:bottom-6 xs:right-1"
                >
                    <div className="animate-bounce">
                        <i className="fa-solid fa-phone text-3xl inline text-white bg-blue-600 px-6 py-5 rounded-full "></i>
                    </div>
                </a>
                <a
                    href="#"
                    className="fixed xl:bottom-3 xl:left-40 xs:bottom-2 xs:left-1"
                >
                    <div>
                        <button className="bg-[#4cd964] xl:p-4 xs:px-2 xs:py-4 flex justify-center items-center rounded-full font-medium text-white">
                            <i className="fa-solid fa-plus xl:text-2xl xs:text-sm font-medium xl:pr-3 xs:pr-1 flex justify-center items-center "></i>
                            CONNECT
                        </button>
                    </div>
                </a>

                <div className="flex justify-center">
                    <img src="https://apps.vinaas.com/file/tgs5EFo1O" />
                </div>
                <h1 className="block pt-4 font-bold text-2xl ">
                    Nguyễn Minh Nhật
                </h1>
                <span className="block mt-4 text-lg font-medium text-gray-600">
                    Intern
                </span>

                <span className="block mt-2 text-lg font-medium text-gray-600">
                    Pix
                </span>

                <div className="mt-10">
                    <div className="gap-12 grid xl:grid-cols-4 lg:grid-cols-4 sm:grid-cols-2 md:grid-cols-2 sm:gap-y-6 xs:grid-cols-2">
                        <a href="#">
                            <button className="bg-white  border rounded-full flex  py-5 justify-center w-full">
                                <div className="flex text-center items-center justify-center ">
                                    <i className="fa-regular fa-envelope text-3xl text-blue-600"></i>
                                    <span className=" font-semibold pl-3 text-blue-600">
                                        EMAIL
                                    </span>
                                </div>
                            </button>
                        </a>
                        <a href="#">
                            <button className="bg-white  border rounded-full flex w-full py-4 justify-center   ">
                                <div className="flex text-center items-center justify-center">
                                    <img
                                        className="w-10 h-10"
                                        src="https://m.ishowroom.vn/assets/zalo.png"
                                    />
                                    <span className=" font-semibold pl-3 text-blue-600 ">
                                        ZALO
                                    </span>
                                </div>
                            </button>
                        </a>

                        <a href="#">
                            <button className=" bg-blue-600  border rounded-full flex w-full py-5  justify-center">
                                <div className="flex text-center items-center justify-center">
                                    <i className="fa-solid fa-phone xl:text-3xl lg:text-3xl md:text-3xl sm:text-3xl inline-block text-white xs:text-lg "></i>

                                    <span className="flex xl:pl-3 xl:font-semibold lg:font-semibold lg:pl-3 md:pl-3 md:font-semibold xs:pl-2 xs:text-base text-white">
                                        0865910100
                                    </span>
                                </div>
                            </button>
                        </a>

                        <a href="#">
                            <button className="bg-white  border rounded-full flex w-full py-5 justify-center ">
                                <div className="flex text-center items-center justify-center">
                                    <i className="inline-block fa-solid fa-plus text-2xl"></i>
                                    <span className="flex pl-3 font-semibold">
                                        CONTACT
                                    </span>
                                </div>
                            </button>
                        </a>
                    </div>
                </div>

                <div className="mt-7 ">
                    <div className=" inline-block px-20 py-7 rounded-xl bg-[#4cd964]">
                        <span className="xl:text-xl lg:text-xl md:text-lg sm:text-lg font-bold block">
                            Văn phòng iCom
                        </span>
                        <span className="xl:text-base lg:text-base md:text-xs sm:text-xs mt-2">
                            42 Mê Linh, Bình Thạnh, TP Hồ Chí Minh
                        </span>
                    </div>
                </div>

                {/* <div className="">
                    <a href="#">
                        <div className=" xl:right-20 sm:top-full sm:-right-8">
                            <i className="fa-solid fa-phone text-3xl inline text-white bg-blue-600 px-6 py-5 rounded-full animate-bounce"></i>
                        </div>
                    </a>
                    <a href="#">
                        <div className="">
                            <button className="bg-[#4cd964] p-4  flex justify-center items-center rounded-full font-medium text-white">
                                <i className="fa-solid fa-plus text-2xl font-medium pr-3 flex justify-center items-center "></i>
                                CONNECT
                            </button>
                        </div>
                    </a>
                </div> */}

                <div className="mt-4 grid xl:grid-cols-2 lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1  gap-5 text-center justify-center">
                    <img
                        className="w-full rounded-2xl"
                        src="https://apps.vinaas.com/file/QvulGGIHK"
                    />
                    <img
                        className="w-full rounded-2xl"
                        src="https://apps.vinaas.com/file/QvulGGIHK"
                    />
                </div>
                <div className="mt-4 font-semibold text-lg">
                    Hình ảnh văn phòng
                </div>

                <div className="mt-10 grid xl:grid-cols-4 lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-10 ">
                    <div className="">
                        <a href="#">
                            <button className="bg-blue-500 w-56 py-4 sm:w-full xs:w-full rounded-full text-white font-semibold text-lg">
                                <span>ĐẶT PHÒNG HỌP</span>
                            </button>
                        </a>
                        <span className="block mt-2 font-medium text-sm text-gray-800">
                            Để tiếp khách bên ngoài
                        </span>
                    </div>

                    <div>
                        <a href="#">
                            <button className="bg-blue-500 w-56 py-4 sm:w-full xs:w-full rounded-full text-white font-semibold text-lg">
                                <span>ĐÀO TẠO THỨ 3</span>
                            </button>
                        </a>
                        <span className="block mt-2 font-medium text-sm text-gray-800">
                            Chiều từ 4h30 tới 5h30
                        </span>
                    </div>

                    <div>
                        <a href="#">
                            <button className="bg-blue-500 w-56 py-4 sm:w-full xs:w-full rounded-full text-white font-semibold text-lg">
                                <span>ĐÀO TẠO THỨ 5</span>
                            </button>
                        </a>
                        <span className="block mt-2 font-medium text-sm text-gray-800">
                            Chiều từ 4h30 tới 5h30
                        </span>
                    </div>

                    <div>
                        <a href="#">
                            <button className="bg-blue-500 w-56 py-4 sm:w-full xs:w-full rounded-full text-white font-semibold text-lg">
                                <span>CHIA SẺ HÀNG TUẦN</span>
                            </button>
                        </a>
                        <span className="block mt-2 font-medium text-sm text-gray-800">
                            Để phát triển cá nhân, tổ chức
                        </span>
                    </div>

                    <div>
                        <a href="#">
                            <button className="bg-blue-500 w-56 py-4 sm:w-full xs:w-full rounded-full text-white font-semibold text-lg">
                                <span>CAFE CUỐI TUẦN</span>
                            </button>
                        </a>
                        <span className="block mt-2 font-medium text-sm text-gray-800">
                            Sáng thứ 7 từ 9h30
                        </span>
                    </div>

                    <div>
                        <a href="#">
                            <button className="bg-blue-500 w-56 py-4 sm:w-full xs:w-full rounded-full text-white font-semibold text-lg">
                                <span>THỂ DỤC THỂ THAO</span>
                            </button>
                        </a>
                        <span className="block mt-2 font-medium text-sm text-gray-800">
                            Tối từ 19h tới 21h
                        </span>
                    </div>

                    <div>
                        <a href="#">
                            <button className="bg-blue-500 w-56 py-4 sm:w-full xs:w-full rounded-full text-white font-semibold text-lg">
                                <span>ĐÀO TẠO THỨ 5</span>
                            </button>
                        </a>
                        <span className="block mt-2 font-medium text-sm text-gray-800">
                            Chiều từ 4h30 tới 5h30
                        </span>
                    </div>

                    <div>
                        <a href="#">
                            <button className="bg-blue-500 w-56 py-4 sm:w-full xs:w-full rounded-full text-white font-semibold text-lg">
                                <span>CHIA SẺ HÀNG TUẦN</span>
                            </button>
                        </a>
                        <span className="block mt-2 font-medium text-sm text-gray-800">
                            Để phát triển cá nhân, tổ chức
                        </span>
                    </div>
                </div>

                <div className="mt-8 font-semibold text-lg text-gray-700 pb-[100px]">
                    ID: 10001001056.5242
                </div>
            </div>
        </>
    );
}

export default App;
