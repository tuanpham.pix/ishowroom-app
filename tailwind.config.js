/** @type {import('tailwindcss').Config} */
export default {
    mode: "jit",
    content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
    theme: {
        extend: {
            screens: {
                xs: "300px", // Thêm breakpoint cho điện thoại nhỏ
            },
        },
    },
    plugins: [],
};
